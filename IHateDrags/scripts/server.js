#!/usr/bin/env node
'use strict'

const Hapi = require('hapi');
const request = require('request');

const server = new Hapi.Server();
server.connection({ 
  host: 'localhost', 
  port: 8000,
  routes: { cors: true }
});

server.route({
  method: 'GET',
  path:'/', 
  handler: function (req, reply) {
    const limit = req.query.limit ? req.query.limit : 100;
    const skip = req.query.skip ? req.query.skip : 0;
    request.get(`https://api.fda.gov/drug/label.json?limit=${limit}&skip=${skip}`)
          .on('response', function(response, body) {
            reply(response);
          })
    
  }
});

// Start the server
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});
