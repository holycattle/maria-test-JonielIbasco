## I HateDrags
### Setup
- Install `yarn` using relevant instructions from https://yarnpkg.com/en/docs/install
- Install dependencies by running `yarn install`
- Run `yarn start-client` to start the webpack dev server
- Run `yarn start-server` to start the Hapi wrapper (used to get around CORS)
### Notes
- You can change the maximum number of elements that will be fetched from the OpenFDA API.
  - The API places a hard limit of >5000, but I set a "soft" limit with the `MAX` constant in App.jsx.
