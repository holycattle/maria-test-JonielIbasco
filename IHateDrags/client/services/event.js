"use strict";

var $            = require("jquery");
var EventEmitter = require("wolfy87-eventemitter");

//TODO: turn this into ES6+

function EventService() {
  function on(evt) {
    checkNull("on", evt);
    ee.on.apply(ee, arguments);
  }

  function off(evt) {
    checkNull("off", evt);
    ee.off.apply(ee, arguments);
  }

  function once(evt) {
    checkNull("once", evt);
    ee.once.apply(ee, arguments);
  }

  function emit(evt) {
    checkNull("emit", evt);
    logging(arguments);
    ee.emit.apply(ee, arguments);
  }

  function error(source, data) {
    emit("error", source, data);
  }

  function checkNull(name, evt) {
    if (!evt) {
      throw "EventService#" + name + " is called without event name.";
    }
  }

  function logging(args) {
    if (debug) {
      var newArgs = Array.prototype.slice.call(args, 1);
      newArgs.unshift("Event - " + args[0]);
      console.log.apply(console, newArgs);
    }
  }

  function debugMode(v) {
    if (v === undefined) {
      return debug;
    } else {
      debug = v;
      return self;
    }
  }

  var self  = this;
  var ee    = new EventEmitter();
  var debug = false;

  ee.on("error", function(source, data) {
    console.log("!!!error!!!", source, data);
  });
  $.extend(this, {
    on: on,
    off: off,
    once: once,
    emit: emit,
    error: error,
    debugMode: debugMode
  });
}

module.exports = new EventService();
