'use strict';

import React from 'react';
import Drug from './components/Drug.jsx'
import drugAPI from './api/fda.js'

//Components:
//Drug - shows each item
//DrugScroll - 
  //DrugSearch
  //DrugFilter

export default class App extends React.Component {
  constructor(args) {
    super(args);
    EventService.on(Events.GET_DRUGS_SUCCESS, this.onGetDrugs);
    drugAPI.getAllDrugLabels();
    console.log("test");
  }
  //initial state
  state = {
    page: 1
  }
  componentWillUnmount() {
    EventService.off(Events.GET_DRUGS_SUCCESS, this.onGetDrugs);
  }
  onGetDrugs = (data) => {
    console.log(data);
    this.setState({page: this.state.page + 1});
  }
  render() {
    <div>
      <Drug />
    </div>;
  }
}
