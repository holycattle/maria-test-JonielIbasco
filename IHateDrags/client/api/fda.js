import API from "./API.js";
import Events from '../constants/event_types.js';
const EventService = require('../services/event.js');
class FDA extends API {
  constructor(args) {
    super(args);
  }
  //TODO: wrap in some sort of API/request object to manage pagination and stuff
  getAllDrugLabels(limit = 100, page = 1) {
    const skip = limit * (page - 1);
    //const skip = page;
    this.exec("GET", `?limit=${limit}&skip=${skip}`).done(function(data) {
      //TODO; refactor this to a Drug class 
      const fdaData = data.results.filter(function (d) {
        return Object.keys(d.openfda).length > 1;
      }).map(function (d) {
        const openfda = d.openfda;
        return {
          spl_id: openfda.spl_id,
          generic_name: openfda.generic_name,
          brand_name: openfda.brand_name,
          product_type: openfda.product_type,
          route: openfda.route,
          purpose: d.purpose,
          indications_and_usage: d.indications_and_usage,
          warnings: d.warnings,
          active_ingredient: d.active_ingredient,
          inactive_ingredient: d.inactive_ingredient,
          storage_and_handling: d.storage_and_handling
        }
      });
      const newMeta = {
        last_updated: data.meta.last_updated,
        limit: data.meta.limit,
        skip: data.meta.skip,
        total: data.meta.total
      };
      const newData = {
        meta: newMeta,
        results: fdaData
      };
      EventService.emit(Events.GET_DRUGS_SUCCESS, newData);
    });
    //TODO: error handling
  }
}

export default new FDA();
