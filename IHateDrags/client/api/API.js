"use strict";

//TODO: convert this to be more ES6-like because this is fucking Frankenstein

const $ = require('jquery');
const EventService = require('../services/event.js');
import Events from '../constants/event_types.js';
class API {
  constructor() {
    //TODO: move this declaration to a config later
    this.endpoint = "http://localhost:8000"
    this.exec = function (method, path, data, ignores) {
      function isIgnore(status) {
        return ignores && ignores[status];
      }
      var d = $.Deferred();
      console.log(this.endpoint + path);
      $.ajax({
        url: this.endpoint + path,
        type: method,
        data: data
      }).done(function(data, status, xhr) {
        d.resolve(data, status, xhr);
      }).fail(function(xhr, status, e) {
        d.reject(xhr, status, e);
        if (isIgnore(xhr.status)) {
          return;
        }
        if (xhr.status === 401) {
          //Force reload
          location.href = "/";
        }
        if (xhr.status === 404) {
          EventService.emit(Events.NOTFOUND, {
            method: method,
            path: path,
            data: data
          });
        }
        console.log("HTTP Request failed with " + xhr.status + ": " + method + " " + path);
        console.log("Request data", data);
        console.log("Response", xhr);
      });
      return d.promise();
    }
    this.mock = function(obj) {
      var d = $.Deferred();
      d.resolve(obj);
      return d.promise();
    }
  }
}

//use this by extending this class, e.g. api/users.js will have a class that extends from API
//but with endpoint specific functions, like login()
//which will make an AJAX call to /users/login

export default API;
