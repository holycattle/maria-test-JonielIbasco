'use strict';

import React from 'react'
import PropTypes from 'prop-types'

export default class Drug extends React.Component {
  static propTypes = {
    drug: PropTypes.object,
    num: PropTypes.number
  };

  static defaultProps = {
    drug: {}
  };

  printProp = (prop, defaultProp="N/A") => {
    //TODO: data is an array, so you need to check if there's more than one property
    //if more than one property, format with commas
    if (!prop) {
      return defaultProp;
    } else {
      return prop.join(", ");
    }
  }

  //TODO: format each row as a grid
  render() {
    const drug = this.props.drug;
    return (
      <div>
        <b>{this.props.num+1}) {drug.spl_id}:</b>
        <div>
          Generic name: <b>{this.printProp(drug.generic_name)}</b>
        </div>
        <div>
          Brand name: <b>{this.printProp(drug.brand_name)}</b>
        </div>
        <div>
          Product type: {this.printProp(drug.product_type)}
        </div>
        <div>
          Route of administration: {this.printProp(drug.route)}
        </div>
        <div>
          Purpose: {this.printProp(drug.purpose)}
        </div>
        <div>
          Indications and usage: {this.printProp(drug.indications_and_usage)}
        </div>
        <div style={{color: "red"}}>
          <b>Warnings:</b> {this.printProp(drug.warnings)}
        </div>
        <div>
          Active ingredients: {this.printProp(drug.active_ingredient)}
        </div>
        <div>
          Inactive ingredients: {this.printProp(drug.inactive_ingredient)}
        </div>
        <div>
          Storage and handling: {this.printProp(drug.storage_and_handling)}
        </div>
      </div>
    );
  }
}
