'use strict';

import React from 'react';

import Drug from '../components/Drug.jsx'
import drugAPI from '../api/fda.js'
import EventService from '../services/event.js';
import Events from '../constants/event_types.js';

const Loading = require('react-loading-animation');

const $ = require('jquery');

const LIMIT = 25;
const MAX = 100; //hard limit of pagination from the API
const FETCH_INTERVAL = 1000;

let scrollableDiv = null;
let drugTimer = null;

export default class App extends React.Component {
  constructor(args) {
    super(args);
    EventService.on(Events.GET_DRUGS_SUCCESS, this.onGetDrugs);
    console.log("mounting");
    drugAPI.getAllDrugLabels(LIMIT, this.state.page + 1);
  }
  //initial state
  state = {
    page: 0,
    drugs: [],
    drugsByRoute: {},
    viewableDrugs: [],
    viewablePage: 0,
    loading: true,
    hasLoadedAll: false,
    routes: [],
    routeFilter: null,
    showNoMoreDiv: false
  }
  componentDidMount() {
    //refactor this to become event-driven
    drugTimer = window.setInterval(this.getMoreDrugs, FETCH_INTERVAL);
  }
  componentWillUnmount() {
    EventService.off(Events.GET_DRUGS_SUCCESS, this.onGetDrugs);
  }
  getMoreDrugs = () => {
    if (this.state.loading) {
      return null;
    }
    const page = this.state.page;
    if (this.state.hasLoadedAll) {
      window.clearInterval(drugTimer);
      return null;
    }
    this.setState({ loading: true });
    drugAPI.getAllDrugLabels(LIMIT, page + 1);
  }
  onGetDrugs = (data) => {
    const drugs = this.state.drugs.concat(data.results);
    console.log(data);
    const drugsByRoute = this.state.drugsByRoute;
    const routes = this.state.routes;
    //TODO: optimize for space by refactoring how drugs are stored and only storing keys here
    //Note: not actually in use
    data.results.forEach(function (d) {
      if (d.route) {
        //some drugs have more than one route
        d.route.forEach(function (r) {
          if (!drugsByRoute[r]) {
            drugsByRoute[r] = [d];
            routes.push(r);
            return null;
          }
          drugsByRoute[r].push(d);
        });
      }
    });
    console.log(drugsByRoute);
    const page = this.state.page;
    const hasLoadedAll = LIMIT * (page+1) > MAX;

    //TODO: make a pagination helper
    const newState = {
      page: page + 1,
      drugs: drugs,
      totalDrugs: data.meta.total,
      loading: false,
      hasLoadedAll: hasLoadedAll,
      drugsByRoute: drugsByRoute,
      routes: routes
    };
    //init
    if (this.state.viewableDrugs.length === 0) {
      newState["viewableDrugs"] = this.state.viewableDrugs.concat(drugs);
      newState["viewablePage"] = this.state.viewablePage + 1;
    }
    this.setState(newState);
  }
  checkScrollBottom = () => {
    if (this.state.viewableDrugs.length >= this.state.drugs.length) {
      return null;
    }

    if (scrollableDiv === null) {
      scrollableDiv = document.getElementById('drugScroll');
    }
    const computedStyle = window.getComputedStyle(scrollableDiv, null);
    if (scrollableDiv.scrollTop < scrollableDiv.scrollHeight - scrollableDiv.offsetHeight) {
      return null;
    }

    const viewablePage = this.state.viewablePage+1;
    //e.g. [0,20), [20-40), [40, 50)
    const end = this.state.viewableDrugs.length + LIMIT + 1;
    const start = this.state.viewableDrugs.length;
    const nextDrugs = this.state.drugs.slice(start, end);
    if (nextDrugs.length === 0) {
      return null;
    }
    this.setState({
      viewableDrugs: this.state.viewableDrugs.concat(nextDrugs),
      viewablePage: this.state.viewablePage+1,
      showNoMoreDiv: this.state.hasLoadedAll
    });
  }
  onChangeRouteFilter = (e) => {
    e.preventDefault();
    this.setState({
      routeFilter: e.target.value
    });
  }
  isParamMatch = (drugVal, val, checkSimilar=false) => {
    if (!drugVal || !val) return true;
    const dVal = drugVal[0].toUpperCase();
    //both indexOf and includes aren't working as expected for substrings
    if (val) return dVal === val || (checkSimilar && dVal.indexOf(val) >= 0)
    else return true;
  }
  //this should probably be refactored to a DrugSet class or something
  filterDrugs = (viewableDrugs, route=null, genericName=null, brandName=null) => {
    if (!route && !genericName && !brandName) return viewableDrugs;
    const isParamMatch = this.isParamMatch;
    const drugFilter = function (d) {
      return isParamMatch(d.route, route) && isParamMatch(d.generic_name, genericName, true)
        && isParamMatch(d.brand_name, brandName, true);
    }
    const viewable = viewableDrugs.filter(drugFilter);
    //search in drugs if viewable is empty
    if (viewable.length === 0) {
      return this.state.drugs.filter(drugFilter);
    }
    return viewable;
  }
  //TODO: turn this into a mixin
  onGenericNameChange = (e) => {
    e.preventDefault();
    const val = e.target.value;
    if (val.length < 3 && val.length > 0) return null; 
    this.setState({
      genericNameFilter: val.toUpperCase()
    });
  }
  onBrandNameChange = (e) => {
    e.preventDefault();
    const val = e.target.value;
    if (val.length < 3 && val.length > 0) return null; 
    this.setState({
      brandNameFilter: val.toUpperCase()
    });
  }
  render() {
    console.log(this.state);
    const viewableDrugs = this.filterDrugs(this.state.viewableDrugs, this.state.routeFilter, this.state.genericNameFilter, this.state.brandNameFilter);
    const styleScroll = {
      overflowY: "auto",
      height: "500px",
      fontFamily: "arial"
    };
    const scrollEnd = this.state.viewableDrugs.length === this.state.drugs.length;
    const isLoading = (this.state.loading || scrollEnd) && !this.state.hasLoadedAll;
    const loadingDiv = !isLoading ? null : (
      <div style={{display: "inline-block", paddingLeft: "50%"}}>
        <div>Loading...</div>
        <Loading/>
      </div>      
    );
    const noMoreDiv = this.state.showNoMoreDiv ?
      <div><b>No more products to display</b></div> : null;
    const inlineStyle = {
      display: "inline-block"
    };
    return (
      <div>
        <select
          style={inlineStyle}
          onChange={this.onChangeRouteFilter}
          value={this.state.route}
          defaultValue="placeholder"
          >
            <option value="placeholder" disabled>Filter by route</option>
            <option value="">Any</option>
            {this.state.routes.map(function (r, idx) {
              return <option value={r} key={idx}>{r}</option>
            })}
        </select>
        <input placeholder="Generic Name"
          onChange={this.onGenericNameChange}/>
        <input placeholder="Brand Name"
          onChange={this.onBrandNameChange}/>
        <div id="drugScroll" style={styleScroll} onScroll={this.checkScrollBottom}>
          {
            viewableDrugs.map(function (d, idx) {
              return (
                <Drug drug={d} num={idx} key={idx}/>
              );
            })
          }
          {noMoreDiv}
          {loadingDiv}
        </div>
      </div>
    );
  }
}
