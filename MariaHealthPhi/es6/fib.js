//reaches Infinity at n = 1476

const getMariaHealth = (x) => {
    if (x % 3 === 0 && x % 5 === 0) {
      return "Maria Health";
    } if (x % 3 === 0) {
      return "Maria";
    } else if (x % 5 === 0) {
      return "Health";
    } else {
      return x;
    }
}

//time complexity: O(n)
//space complexity: O(n)
const getFib2 = (n) => {
  if (n === 1) {
    return [n];
  } else if (n === 2) {
    return [1, 1];
  } else if (n <= 0) {
    return [];
  }
  const output = [1, 1];
  while (output.length < n) {
    if (output[output.length-1]+output[output.length-2] === Infinity) {
      console.log("Infinity reached at "+output.length);
      break;
    }
    output.push(output[output.length-1]+output[output.length-2]);
  }
  return output.map((x) => {
    return getMariaHealth(x);
  });
}
console.log("JS fib");
console.log("n=-1: "+getFib2(-1));
console.log("n=0: "+getFib2(0));
console.log("n=1: "+getFib2(1));
console.log("n=2: "+getFib2(2));
console.log("n=3: "+getFib2(3));
console.log("n=12: "+getFib2(12));
console.log("n=25: "+getFib2(20));
