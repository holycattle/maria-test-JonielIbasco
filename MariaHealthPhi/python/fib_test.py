from fib import get_fib, get_output
#test for first 12 Fibonacci numbers
FIB = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]

FIB_LEN_ERROR = "Output doesn't have the expected number of Fibonacci numbers!"
PRINT_LEN_ERROR = "Output doesn't have the expected number of elements! {}!={}"
OUTPUT_MISMATCH_ERROR = "{} doesn't match your output {}"

def test_get_fib(n):
    print("Testing get_fib at n = {}... ".format(n))
    l = get_fib(n)
    if n == 0:
        assert len(l) == 1, FIB_LEN_ERROR
        assert l[0] == 0, OUTPUT_MISMATCH_ERROR
        print("Done!")
        return
    elif n < 0:
        assert len(l) == 0, PRINT_LEN_ERROR.format(len(l), 0)
        print("Done!")
        return
 
    expected = 0
    actual = 0
    assert len(l) == n, FIB_LEN_ERROR
    for i in range(n):
        expected = FIB[i]
        actual = l[i]
        assert actual == expected, OUTPUT_MISMATCH_ERROR.format(expected, actual)
    print("Done!")

def test_get_output(n):
    print("Testing print_output at n = {}... ".format(n))
    l = get_output(get_fib(n))
    expected = 0
    actual = 0
    if n >= 0:
        a = (n if n > 0 else 1)
        assert len(l) == a, PRINT_LEN_ERROR.format(len(l), n if n > 0 else 1)
    else:
        assert len(l) == 0, PRINT_LEN_ERROR.format(len(l), 0)
    for i in range(n):
        expected = FIB[i]
        actual = l[i]
        if expected % 3 == 0:
            assert actual == "Maria", OUTPUT_MISMATCH_ERROR.format("Maria", str(actual))
        elif expected % 5 == 0:
            assert actual == "Health", OUTPUT_MISMATCH_ERROR.format("Health", str(actual))
        elif expected % 3 == 0 and expected % 5 == 0:
            assert actual == "Maria Health", OUTPUT_MISMATCH_ERROR.format("Maria Health", str(actual))
        else:
            assert actual == str(expected), OUTPUT_MISMATCH_ERROR.format(expected, actual)
    print("Done!")

if __name__ == "__main__":
    test_get_fib(-1)
    test_get_fib(0)
    test_get_fib(1)
    test_get_fib(7)
    test_get_fib(12)

    test_get_output(-1)
    test_get_output(0)
    test_get_output(1)
    test_get_output(7)
    test_get_output(12)
