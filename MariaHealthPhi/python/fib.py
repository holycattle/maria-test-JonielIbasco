#returns a list for testability
def get_fib(n):
    if n == 1:
        return [1]
    elif n <= 0:
        return [] 
    s = 0
    prev = 1
    curr = 1
    res = []
    for i in range(n):
        s = prev + curr
        if i < 2:
            res.append(1)
            continue
        else:
            res.append(s)
        prev = curr
        curr = s 

    return res

#returns a list for testability
def get_output(l):
    res = []
    o = None
    for x in l:
        if x % 3 == 0 and x % 5 == 0:
            o = "Maria Health"
        elif x % 3 == 0:
            o = "Maria"
        elif x % 5 == 0:
            o = "Health"
        else:
            o = str(x)
        res.append(o)
    return res

if __name__ == "__main__":
    print("Python fib")
    print("n=-1: "+str(get_output(get_fib(-1))))
    print("n=0: "+str(get_output(get_fib(0))))
    print("n=1: "+str(get_output(get_fib(1))))
    print("n=2: "+str(get_output(get_fib(2))))
    print("n=3: "+str(get_output(get_fib(3))))
    print("n=12: "+str(get_output(get_fib(12))))
    print("n=25: "+str(get_output(get_fib(20))))
