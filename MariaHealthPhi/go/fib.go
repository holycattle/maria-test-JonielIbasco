package main

import "fmt"
import "strconv"

func getMariaHealth(n int) string {
    if n % 3 == 0 && n % 5 == 0 {
        return "Maria Health"
    } else if n % 3 == 0 {
        return "Maria"
    } else if n % 5 == 0 {
        return "Health"
    } else {
        return strconv.Itoa(n)
    }
}

//Overflows at n = 93
func Fib(n int) []string {
    if n == 1 {
        return []string{strconv.Itoa(n)}
    } else if n <= 0 {
        return nil
    }
    a := 1
    b := 1
    c := 0
    l := make([]string, n, n)
    l[0] = strconv.Itoa(1);
    l[1] = strconv.Itoa(1);
    for i := 2; i < n; i++ {
        //stop once an overflow has happened
        c = a+b
        if a+b < 0 {
            fmt.Println("Overflow:",i+1)
            return l
        }
        l[i] = getMariaHealth(c)
        a = b
        b = c
    }
    return l
}

func main() {
    fmt.Println("Golang fib")
    fmt.Println(Fib(-1))
    fmt.Println(Fib(0))
    fmt.Println(Fib(1))
    fmt.Println(Fib(2))
    fmt.Println(Fib(3))
    fmt.Println(Fib(12))
    fmt.Println(Fib(20))
    //fmt.Println(Fib(92))
    //fmt.Println(Fib(93))
}
