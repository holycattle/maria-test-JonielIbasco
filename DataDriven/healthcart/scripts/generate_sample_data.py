from cart.serializers import Plan, Provider, PlanPaymentTerm 

# Providers
print("Creating sample providers...")
maria = Provider(name='MariaHealth', country='PH')
minicare = Provider(name='MiniCare', country='PH')
stupicare = Provider(name='Stupicare', country='PH')
maria.save()
minicare.save()
stupicare.save()

# Plans
print("Creating sample plans...")
mh_basic = Plan(name='MariaHealthBasic', provider=maria)
mc_bronze = Plan(name='MiniCareBronze', provider=minicare)
sc_plat = Plan(name='StupicarePlatinum', provider=stupicare)
mh_basic.save()
mc_bronze.save()
sc_plat.save()

# PlanPaymentTerm
PlanPaymentTerm(term=PlanPaymentTerm.MONTHLY, plan=mh_basic, cost=1000).save()
PlanPaymentTerm(term=PlanPaymentTerm.QUARTERLY, plan=mh_basic, cost=750).save()
PlanPaymentTerm(term=PlanPaymentTerm.ANNUALLY, plan=mh_basic, cost=500).save()
PlanPaymentTerm(term=PlanPaymentTerm.MONTHLY, plan=mc_bronze, cost=2000).save()
PlanPaymentTerm(term=PlanPaymentTerm.ANNUALLY, plan=sc_plat, cost=20001).save()
