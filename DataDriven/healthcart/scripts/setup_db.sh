#!/usr/bin/env bash
echo $1
sudo -u $1 psql -c "CREATE USER maria WITH PASSWORD 'health';"
sudo -u $1 psql -c "CREATE DATABASE health_cart;"
sudo -u $1 psql -c "GRANT ALL PRIVILEGES ON DATABASE health_cart to maria;"
python manage.py migrate
python manage.py shell < scripts/generate_sample_data.py
