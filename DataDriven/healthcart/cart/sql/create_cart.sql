DROP TABLE IF EXISTS providers CASCADE;
DROP TABLE IF EXISTS plans CASCADE;
DROP TABLE IF EXISTS plan_payment_terms CASCADE;
DROP TABLE IF EXISTS carts CASCADE;
DROP TABLE IF EXISTS cart_items CASCADE;

CREATE TABLE providers (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  country varchar(4) NOT NULL
);

CREATE TABLE plans (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL UNIQUE,
  description varchar NULL,
  provider_id INTEGER NOT NULL REFERENCES providers (id) ON DELETE CASCADE,
  status SMALLINT NOT NULL DEFAULT '0',
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE plan_payment_terms (
  id SERIAL PRIMARY KEY,
  plan_id INTEGER NOT NULL REFERENCES plans(id) ON DELETE CASCADE,
  payment_term SMALLINT NOT NULL DEFAULT '0',
  cost REAL NOT NULL,
  status SMALLINT NOT NULL DEFAULT '0',
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (plan_id, payment_term)
);

CREATE TABLE carts (
  id SERIAL PRIMARY KEY,
  token varchar(64) NOT NULL UNIQUE,
  status SMALLINT NOT NULL DEFAULT '0',
  cost INTEGER NOT NULL DEFAULT 1,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE cart_items (
  id SERIAL PRIMARY KEY,
  cart_id INTEGER NOT NULL REFERENCES carts (id) ON DELETE CASCADE,
  count INTEGER NOT NULL DEFAULT 1,
  plan_payment_term_id INTEGER NOT NULL
    REFERENCES plan_payment_terms (id) ON DELETE CASCADE,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (cart_id, plan_payment_term_id)
);
