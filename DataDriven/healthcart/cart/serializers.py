from rest_framework import serializers
from cart.models import Provider, Plan, PlanPaymentTerm, Cart, CartItem

class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields = ('name', 'country')

# gives no info about its Plan parent; can only be used in the context of a Plan
class PlanPaymentTermSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlanPaymentTerm
        fields = ('id', 'term', 'status', 'cost', 'created_on', 'updated_on')

class PlanSerializer(serializers.ModelSerializer):
    plan_payment_terms = PlanPaymentTermSerializer(many=True, read_only=True)
    provider = ProviderSerializer(read_only=True)
    #provider_id = serializers.PrimaryKeyRelatedField(queryset=Provider.objects.all())
    class Meta:
        model = Plan
        fields = ('id', 'name', 'description', 'provider', 'created_on', 'updated_on', 'status', 'plan_payment_terms')

    '''
    def create(self, validated_data):
        plan_payment_terms_data = validated_data.pop('plan_payment_terms')
        validated_data['provider_id'] = validated_data.pop('provider_id').id
        plan = Plan.objects.create(**validated_data) 
        for plan_payment_term in plan_payment_terms_data:
            PlanPaymentTerm.objects.create(plan=plan, **plan_payment_term)
        return plan

    def update(self, instance, validated_data):
        plan_payment_terms_data = validated_data.pop('plan_payment_terms')
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.provider = validated_data.get('provider_id', instance.provider)
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        ppt = PlanPaymentTerm.objects.filter(plan_id=instance.id)
        existing_terms = [p.term for p in ppt]
        input_terms = [ppt_data['term'] for ppt_data in plan_payment_terms_data]
        for plan_payment_term in plan_payment_terms_data:
            e = ppt.update_or_create(plan=instance, **plan_payment_term)
            e[0].save()
        deleted_terms = set(existing_terms) - set(input_terms)
        return instance
    '''

class CartItemPaymentTermIDSerializer(serializers.ModelSerializer):
    plan_payment_term_id = serializers.PrimaryKeyRelatedField(queryset=PlanPaymentTerm.objects.all())
    class Meta:
        model = CartItem
        fields = ('plan_payment_term_id', 'count', 'created_on')

class CartItemSerializer(serializers.ModelSerializer):
    plan_payment_term = PlanPaymentTermSerializer(read_only=True)
    class Meta:
        model = CartItem
        fields = ('plan_payment_term', 'count', 'created_on')

# use only for creating a Cart entry
class CartWriteSerializer(serializers.ModelSerializer):
    # TODO: support writing to cart_items
    cart_items = CartItemPaymentTermIDSerializer(many=True)
    class Meta:
        model = Cart
        # don't include token, for obvious security reasons
        fields = ('cost', 'status', 'created_on', 'cart_items', 'token')

    def create(self, validated_data):
        cart_items_data = validated_data.pop('cart_items')
        cart = Cart.objects.create(**validated_data)
        cart.cost = sum([c['plan_payment_term_id'].cost * c['count'] for c in cart_items_data])
        for ci_data in cart_items_data:
            ci_data['plan_payment_term_id'] = ci_data['plan_payment_term_id'].id
            CartItem.objects.create(cart=cart, **ci_data)
        return cart

    # TODO: might be better to refactor some business logic to the views
    def update(self, instance, validated_data):
        cart_items_data = validated_data.pop('cart_items')
        if 'status' in validated_data:
            instance.status = validated_data['status']
        ci = CartItem.objects.filter(cart_id=instance.id)
        existing_term_ids = [c.plan_payment_term.id for c in ci]
        # compute cost of cart
        instance.cost = sum([c['plan_payment_term_id'].cost * c['count'] for c in cart_items_data])
        input_term_ids = [ppt_data['plan_payment_term_id'].id for ppt_data in cart_items_data]
        # TODO: find a way to optimize/batch these queries? how to ORM ;_;
        for ci_data in cart_items_data:
            term = ci_data.pop('plan_payment_term_id')
            item = ci.get_or_create(cart=instance, plan_payment_term=term)[0]
            # if there are any changes to the item count, update
            if 'count' in ci_data:
                item.count = ci_data['count']
                item.save()
        deleted_term_ids = list(set(existing_term_ids) - set(input_term_ids))
        if len(deleted_term_ids) > 0:
            ci.filter(plan_payment_term_id__in=deleted_term_ids).delete()
        instance.save()
        return instance

class CartReadSerializer(serializers.ModelSerializer):
    cart_items = CartItemSerializer(many=True)
    class Meta:
        model = Cart
        # don't include token, for obvious security reasons
        fields = ('cost', 'status', 'created_on', 'updated_on', 'cart_items')
