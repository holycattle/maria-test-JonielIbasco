from cart.serializers import ProviderSerializer, PlanPaymentTermSerializer, PlanSerializer,\
        CartItemSerializer, CartReadSerializer, CartWriteSerializer
from cart.models import Provider, Plan, PlanPaymentTerm, Cart, CartItem
from rest_framework import status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view

import healthcart.settings as settings

from datetime import datetime
import hashlib

def get_error_message(code, messages):
    return {'code':code, 'errors':messages}

# GET /
@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'plans': reverse('plan-list', request=request, format=format),
    })
# GET /plans
class PlanViewSet(viewsets.ModelViewSet):
    """
    Retrieve list of plans
    """ 
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer

plan_list = PlanViewSet.as_view({
    'get': 'list'
})

# GET /carts/mine
def get_cart(request):
    try:
        cart = Cart.objects.get(token=request.data['token'])
        serializer = CartReadSerializer(cart)
        return Response(serializer.data)
    except Cart.DoesNotExist:
        return Response(status=status.HTTP_403_FORBIDDEN)

# PUT /carts/mine
def update_cart(request):
    try:
        # can only update pending carts
        cart = Cart.objects.get(token=request.data['token'], status=Cart.PENDING)
        serializer = CartWriteSerializer(cart, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    except Cart.DoesNotExist:
        error = get_error_message(403, ['Cart is not available.'])
        return Response(error, status=status.HTTP_403_FORBIDDEN)
    return Response(get_error_message(400, ['Bad request']), status=status.HTTP_400_BAD_REQUEST)

# POST /carts/mine
def create_cart(request):
    # generate random string to be thrown back to user
    # using secret key, latest cart ID+1, and current timestamp
    latest_cart_id = 0
    try:
        latest_cart_id = Cart.objects.latest('id').id + 1
    except Cart.DoesNotExist:
        latest_cart_id = 0
    m = hashlib.sha256()
    print("{}_{}_{}".format(settings.SECRET_KEY, str(datetime.now()), latest_cart_id))
    s = "{}_{}_{}".format(settings.SECRET_KEY, str(datetime.now()), latest_cart_id)
    m.update(s.encode('utf-8'))
    request.data['token'] = m.hexdigest()
    print(m.hexdigest())
    serializer = CartWriteSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# /carts/mine
@api_view(['GET', 'POST', 'PUT'])
def my_cart(request):
    if request.method == 'POST':
        return create_cart(request)

    if 'token' not in request.data:
        resp = get_error_message(403, ['A token is required'])
        return Response(resp, status=status.HTTP_403_FORBIDDEN)

    if request.method == 'GET':
        return get_cart(request)
    if request.method == 'PUT':
        return update_cart(request)

# /carts/mine/checkout
@api_view(['POST'])
def checkout_cart(request):
    if 'token' not in request.data:
        resp = get_error_message(403, ['A token is required'])
        return Response(resp, status=status.HTTP_403_FORBIDDEN)
    try:
        # can only update pending carts
        cart = Cart.objects.get(token=request.data['token'], status=Cart.PENDING)
        cart.status = Cart.PAID
        cart_items = CartItem.objects.filter(cart_id=cart.id)
        cart.cost = sum([c.plan_payment_term.cost * c.count for c in cart_items])
        cart.save()
        serializer = CartReadSerializer(cart)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Cart.DoesNotExist:
        error = get_error_message(403, ['Cart is not available.'])
        return Response(error, status=status.HTTP_403_FORBIDDEN)
    return Response(get_error_message(400, ['Bad request']), status=status.HTTP_400_BAD_REQUEST)
