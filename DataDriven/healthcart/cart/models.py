from django.db import models

# Create your models here.

class Provider(models.Model):
    name = models.CharField(max_length=255)
    country = models.CharField(max_length=4)

class Plan(models.Model):
    INACTIVE = 0
    ACTIVE = 1
    PLAN_STATUS_CHOICES = (
        (INACTIVE, 'inactive'),
        (ACTIVE, 'active')
    )

    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    status = models.PositiveSmallIntegerField(default=ACTIVE, choices=PLAN_STATUS_CHOICES)

class PlanPaymentTerm(models.Model):
    MONTHLY = 0
    QUARTERLY = 1
    ANNUALLY = 2
    TERMS = (
        (MONTHLY, 'monthly'),
        (QUARTERLY, 'quarterly'),
        (ANNUALLY, 'annually')
    )
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name='plan_payment_terms')
    term = models.PositiveSmallIntegerField(default=MONTHLY, choices=TERMS)
    status = models.PositiveSmallIntegerField(default=Plan.ACTIVE, choices=Plan.PLAN_STATUS_CHOICES)
    cost = models.PositiveIntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    #TODO: denormalize provider_id from plan_id for easy reference
    #provider_id = models.ForeignKey(Provider, on_delete=models.CASCADE)
    class Meta:
        unique_together = ('plan', 'term')

class Cart(models.Model):
    PENDING = 0
    PAID = 1
    CANCELLED = 2
    EXPIRED = 3
    CART_STATUS_CHOICES = (
        (PENDING, 'pending'),
        (PAID, 'paid'),
        (CANCELLED, 'cancelled'),
        (EXPIRED, 'expired')
    )
    token = models.CharField(max_length=64, unique=True)
    status = models.PositiveSmallIntegerField(default=PENDING, choices=CART_STATUS_CHOICES)
    #TODO: add cost field
    #denormalized from CartItem, to be computed when Cart is updated (e.g. check out/new item)
    cost = models.PositiveIntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name='cart_items')
    plan_payment_term = models.ForeignKey(PlanPaymentTerm, on_delete=models.CASCADE)
    count = models.PositiveIntegerField(default=1)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('cart', 'plan_payment_term')

