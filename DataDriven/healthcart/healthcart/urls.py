from django.conf.urls import url
from django.contrib import admin

from cart.views import PlanViewSet, api_root, my_cart, checkout_cart, plan_list
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from django.http import JsonResponse

urlpatterns = format_suffix_patterns([
    url(r'^$', api_root),
    # endpoint for retrieving all plans
    url(r'^plans/$', plan_list, name='plan-list'),
    # endpoint for creating, retrieving, and updating your cart
    # also handles updating your cart's items
    url(r'^carts/mine/$', my_cart, name='cart-mine'),
    # endpoint for setting your cart's status to Cart.PAID
    url(r'^carts/mine/checkout/$', checkout_cart, name='cart-checkout')
])
