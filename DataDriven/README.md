# Maria Health Cart
A quick prototype because someone got pwned by a Chinese hacker named Len Ng.

## Prerequisites
Make sure to have Python 3.6, virtualenv, and Postgres installed.

## Setting up the dev environment
### virtualenv
Assuming you have virtualenv for Python3.6 installed, run the following in the root project folder:
```
virtualenv env
source env/bin/activate
```
This will create and activate a virtual environment for development.
If you would like to revert to your system's environment, simply run `deactivate`.
### Python dependencies
Once your virtualenv is active, you can run `pip install -r requirements.txt` to install Python dependencies.
### Database
For convenience, you can run the following from your project directory (i.e. `DataDriven`) to set up an empty database assigned to a role named `maria` with the password `health`, apply Django migrations, and populate it with sample data:
```
    cd healthcart
    ./scripts/setup_db.sh <postgres_admin_role>
```
where `<postgres_admin_role>` is a role that has permissions to create databases and create roles.

## Running the Django app
In the same directory you were in (`DataDriven/healthcart`) when setting up the database, run
```
python manage.py runserver
```
Assuming everything was set up properly, this should start a dev server at `localhost:8000`

## Sample HTTP requests to the server
This assumes you have `curl` installed.
### Viewing all Plans
Returns a list of all Plans and information about the Provider of each Plan.
#### Request
```
curl -H "Content-Type: application/json" -X GET 'localhost:8000/plans/'
```
#### Response format
```
{
   "count":3,
   "next":null,
   "previous":null,
   "results":[
      {
         "id":1,
         "name":"MariaHealthBasic",
         "description":null,
         "provider":{
            "name":"MariaHealth",
            "country":"PH"
         },
         "created_on":"2017-04-24T09:52:27.830188Z",
         "updated_on":"2017-04-24T09:55:49.618344Z",
         "status":1,
         "plan_payment_terms":[
            {
               "id":1,
               "term":0,
               "status":1,
               "cost":1000,
               "created_on":"2017-04-24T10:01:59.866963Z",
               "updated_on":"2017-04-24T10:01:59.867002Z"
            },
            {
               "id":2,
               "term":1,
               "status":1,
               "cost":750,
               "created_on":"2017-04-24T10:02:17.990173Z",
               "updated_on":"2017-04-24T10:02:17.990210Z"
            },
            {
               "id":3,
               "term":2,
               "status":1,
               "cost":500,
               "created_on":"2017-04-24T10:02:30.642053Z",
               "updated_on":"2017-04-24T10:02:30.642091Z"
            }
         ]
      },
      {
         "id":3,
         "name":"MiniCareBronze",
         "description":null,
         "provider":{
            "name":"MiniCare",
            "country":"PH"
         },
         "created_on":"2017-04-24T09:58:36.237655Z",
         "updated_on":"2017-04-24T09:58:36.237684Z",
         "status":1,
         "plan_payment_terms":[
            {
               "id":5,
               "term":0,
               "status":1,
               "cost":2000,
               "created_on":"2017-04-24T10:03:50.279469Z",
               "updated_on":"2017-04-24T10:03:50.279521Z"
            }
         ]
      },
      {
         "id":4,
         "name":"StupidCarePlatinum",
         "description":null,
         "provider":{
            "name":"Stupicare",
            "country":"PH"
         },
         "created_on":"2017-04-24T09:59:42.195322Z",
         "updated_on":"2017-04-24T09:59:42.195558Z",
         "status":1,
         "plan_payment_terms":[
            {
               "id":6,
               "term":2,
               "status":1,
               "cost":3000,
               "created_on":"2017-04-24T10:04:31.098709Z",
               "updated_on":"2017-04-24T10:04:31.098763Z"
            }
         ]
      }
   ]
}
```
### Creating a Cart
This endpoint returns a unique token that will be required for all future read/update operations on the cart.
#### Request format
```
curl -H "Content-Type: application/json" -X POST 'localhost:8000/carts/mine/' -d '{"cart_items":[{"plan_payment_term_id":17, "count":3}]}'
```
#### Request payload
`plan_payment_term_id`: corresponds to an entry in the PlanPaymentTerm model/table
`count`: number of times this item is ordered
```
{
   "cart_items":[
      {
         "plan_payment_term_id":1,
         "count":3
      },
      {
         "plan_payment_term_id":2,
         "count":1
      }
   ]
}
```
#### Response format
`cost`: total price of the cart based the cost of each item multipled by # of times ordered
```
{
   "cost":3000,
   "status":0,
   "created_on":"2017-04-24T10:14:44.048174Z",
   "cart_items":[
      {
         "plan_payment_term_id":1,
         "count":3,
         "created_on":"2017-04-24T10:14:44.053729Z"
      }
   ],
   "token":"b10c5ee374a19b0d486cdf2116213e017fab7490a50c126db39cb5dbb80d1337"
}
```
### Retrieving a Cart
Given a unique token identified, returns a Cart and all associated CartItems.
#### Request format
```
curl -H "Content-Type: application/json" -X GET 'localhost:8000/carts/mine/' -d '{"token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"}'
```
#### Request payload
```
{
   "token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"
}
```
#### Response format
```
{
   "cost":3000,
   "status":0,
   "created_on":"2017-04-24T10:18:29.820111Z",
   "updated_on":"2017-04-24T10:18:29.820142Z",
   "cart_items":[
      {
         "plan_payment_term":{
            "id":1,
            "term":0,
            "status":1,
            "cost":1000,
            "created_on":"2017-04-24T10:01:59.866963Z",
            "updated_on":"2017-04-24T10:01:59.867002Z"
         },
         "count":3,
         "created_on":"2017-04-24T10:18:29.833878Z"
      }
   ]
}
```
### Updating a Cart
Given a unique token identified, updates a Cart and all associated CartItems.

The server compares the items present in the `cart_items` list in the request to the list that exists in the database and handles changes accordingly.
#### Request format
##### Updating the number of orders for a given PlanPaymentTerm
```
curl -H "Content-Type: application/json" -X PUT 'localhost:8000/carts/mine/' -d '{"cart_items":[{"plan_payment_term_id":1, "count":1}], "token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"}'
```
##### Removing all orders in the cart
```
curl -H "Content-Type: application/json" -X PUT 'localhost:8000/carts/mine/' -d '{"cart_items":[], "token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"}'
```
#### Request payload
```
{
   "cart_items":[
      {
         "plan_payment_term_id":1,
         "count":1
      }
   ],
   "token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"
}
```
#### Response format
```
{"cost":1000,"status":0,"created_on":"2017-04-24T10:18:29.820111Z","cart_items":[{"plan_payment_term_id":1,"count":1,"created_on":"2017-04-24T10:18:29.833878Z"}],"token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"}
```
### Marking a Cart as `Paid`
#### Request format
```
curl -H "Content-Type: application/json" -X POST 'localhost:8000/carts/mine/checkout/' -d '{"token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"}'
```
#### Request payload
```
{
   "token":"94afc153bd3b117d90e7470f29ff245a9e359f714fdcd3c4cd846338abcf3910"
}
```
#### Response format
```
{
   "cost":1000,
   "status":1,
   "created_on":"2017-04-24T10:18:29.820111Z",
   "updated_on":"2017-04-24T10:37:17.822597Z",
   "cart_items":[
      {
         "plan_payment_term":{
            "id":1,
            "term":0,
            "status":1,
            "cost":1000,
            "created_on":"2017-04-24T10:01:59.866963Z",
            "updated_on":"2017-04-24T10:01:59.867002Z"
         },
         "count":1,
         "created_on":"2017-04-24T10:18:29.833878Z"
      }
   ]
}
```
